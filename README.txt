Entity Translation Panels IPE
=============================

Integrates Entity Translation with Panels IPE.

- Allow edition of the existing translations directly from the IPE edit dialog.
- Adds a translation button to the Panels IPE that displays the user the
  translation overview page. The button open a dialog similar to the usual
  "Translate" tab for an entity. Currently, the links inside the tab open in a
  new browser tab.


Icon by Icons8 (http://icons8.com/)